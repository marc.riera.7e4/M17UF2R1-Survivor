using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterControllerPlayer : MonoBehaviour
{
    public bool unlimitedJumps = false;
    public bool DoubleJumpsUnlocked;
    private CharacterController controller;
    private Vector3 playerVelocity;
    private Animator ani;
    public bool groundedPlayer;
    public float playerSpeed = 4.0f;
    public float jumpHeight = 1.0f;
    public float gravityValue = -9.81f;
    public float GroundedOffset = -0.14f;
		[Tooltip("The radius of the grounded check. Should match the radius of the CharacterController")]
	public float GroundedRadius = 0.28f;
		[Tooltip("What layers the character uses as ground")]
	public LayerMask GroundLayers;

    private bool jumpAvailable = true;
    private bool doubleJumpAvailable = true;
    private float radiusOriginal;

    public float SlideBorderDistance;
    public float RaycastDistance;
    public float SlideStrenght;
    private RaycastHit hit;

    private Ray rayLeft;
    private Ray rayRight;
    public bool rayTouchLeft;
    public bool rayTouchRight;
    private float originalStepOffset;
    private void Start()
    {
        controller = gameObject.GetComponent<CharacterController>();
        ani = GetComponent<Animator>();
        radiusOriginal = controller.radius;
        originalStepOffset = controller.stepOffset;
    }

    async void Update()
    {
        if (groundedPlayer && playerVelocity.y < 0)
        {
            playerVelocity.y = 0f;
        }
        
        Vector3 spherePosition = new Vector3(transform.position.x, transform.position.y - GroundedOffset, transform.position.z);
			groundedPlayer = Physics.CheckSphere(spherePosition, GroundedRadius, GroundLayers, QueryTriggerInteraction.Ignore);
        

        Vector3 move = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
        controller.Move(move * Time.deltaTime * playerSpeed);

        if (move != Vector3.zero)
        {
            gameObject.transform.forward = move;
            ani.SetBool("run", true);
        }
    
        
        if (move == Vector3.zero && Input.GetButton("Horizontal") == false)
        {
            ani.SetBool("run", false);
        }
        

        // Changes the height position of the player..
        if (Input.GetButtonDown("Jump") && groundedPlayer == true && jumpAvailable == true || Input.GetButtonDown("Jump") && unlimitedJumps == true)
        {
            jumpAvailable = false;
            ani.SetTrigger("jump");
        }

         if (Input.GetButtonDown("Jump") && groundedPlayer == false && doubleJumpAvailable == true && DoubleJumpsUnlocked == true)
        {
            doubleJumpAvailable = false;
            ani.SetTrigger("jump");
        }

        playerVelocity.y += gravityValue * Time.deltaTime;

        if(playerVelocity.y <= gravityValue*1.5){
            playerVelocity.y = gravityValue*1.5f;
        }

        controller.Move(playerVelocity * Time.deltaTime);

        if (groundedPlayer == true){
            ani.SetBool("onair", false);
            doubleJumpAvailable = true;
            controller.radius = radiusOriginal;
            playerVelocity.x = 0;
            

            controller.stepOffset = originalStepOffset;

        } else {
            ani.SetBool("onair", true);
            jumpAvailable = true;
            controller.stepOffset = 0;
        }
    }

    public void StartJumpForce(){
        playerVelocity.y = 0;
        playerVelocity.y += Mathf.Sqrt(jumpHeight * -3.0f * gravityValue);
        
    }
    public void ResetJumpBool(){
        jumpAvailable = true;
    }
}
